Weslley - Hello G.! We stil have some questions, do you mind answering a few more questions regarding to the StackOverflow?
G. - Hello Weslley! No, I don't mind, I can answer them.

Weslley - Did you find some question on StackOverflow in Portuguese that you knew the answer?
G. - No, the results for the search that I usually made were related to what I was looking for. Thus, I didn't know the answer.

Weslley - But have you searched for any questions that you already knew the answer, just to try to answer them?
G. - I was considering to contribute to the StackOverflow in Portuguese. I decided to do so, after our last interview, but I must confess I have not started yet.

Weslley - That would be my next question, I remember that you criticized the small amount of content on the Portugues version of StackOverflow. What do you think about creating new content, asking and answering them, perhaps translating the content available in English to Portuguese?
G. - I got the same idea, I found this idea very interesting. 

Weslley - Do you feel motivated to help other people in StackOverflow?
G. - Yes!

Weslley - You feel motivated to help others on StackOverflow, but you never helped. Why?
G. - Usually I make some search in StackOverflow just to solve my problems. It never came to my mind, that I could help others. I had already contributed in mailing list, but I had never thought that I could contribute to the StackOverflow.

Weslley - Which mailing list did you participate?
G. - Iptables and shellscript from yahoo, and ipv6 from cgi.br.

Weslley - So you already helped the community, I mean, you used to contribute to the mailing list of these projects, but you have never contributed to the StackOverflow. Do you have any specific reason for not contributed to the StackOverflow?
G. - I am not able to identify a specific reason, but I think the reason might be inattention, carelessness.

Weslley - Do you feel more motivated to participate in the mailing list than in StackOverflow?
G. - I don't think so. The amount of email is quite big, I have to read them to check how I can help them. Thus, I think the work would be quite the same at StackOverflow.

Weslley - Hum, ok! Will you continue access the Portugues version of StackOverflow or did you give it up?
G. - I will access the Portugues version and I have plans to try to answer one question per day.

Weslley - Now, when you have a problem that needs solving, in which version of the StackOverflow do you do the search?
G. - First in Portuguese, if I get no answer, then I access the English version.

Weslley - So, until now, the Portuguese version is your first option.
G. - yes. Since when I knew this service was available, I preferer the Portuguese version.

Weslley - Do you feel motivated to help the StackOverflow in English?
G. - I think so, but I will try to help the Portuguese version first.

Weslley - So, do you feel more motivated to help the Portuguese version than English one?!
G. - yes!

Weslley - Would you know why?
G. - The English version is older and it has more people contributing. So, I think it would be more useful if I try to help people who use the Portuguese version.

Weslley - Which language was used in mailing list?
G. - It had not an oficial language, you could see several languages.

Weslley - Which language did you participate?
G. - Portuguese and English.

Weslley - Ok! I think that is all for today. Thank you again for your help.
G. - You are welcome.

Weslley - Bye.
G. - Bye.