				Weslley - Hi Giovanni, good night! This interview is being recorded. I wonder if do you authorize this recording?
				Giovanni - Yes, I do.
				
				Weslley - Okay, let's start! This interview aims identifying some aspects of the behavior of the StackOverflow users, both in the English version and the Portuguese version. I wonder, how do you use the StackOverflow?
				
				Giovanni - Usually I access the StackOverflow when I have programming or database doubts. I try to find out if anyone else has experienced that problem, so I can test the given solution.
				
				Weslley - Do you use both the Portuguese version and the English version? Which version do you use more?
				Giovanni - The English version is the one I use more. I hardly found anything useful to solve my doubts in the Portuguese version.
				
				Weslley - Have you made any question on StackOverflow in Portuguese?
				Giovanni - No I have not.
				
				Weslley - And in the English version?
				Giovanni - I had made one question, but I did not get an answer so far.
				
				Weslley - Do you have an user account in the both of versions?
				Giovanni - Yes, I do.
				
				Weslley - When you used the Portuguese version, why did you use the Portugues version instead of the English one?
				Giovanni - I did it just for curiosity. When I found out that there is StackOverflow in Portuguese, I accessed it to see if it had anything interesting. when I got an opportunity to do the search to solve a problem, first I tried to find the answer in Portuguese. But it had a lot of open questions.
				
				Weslley - Do you think that there are any topic or subject, that it is easier to be found in the Portuguese version than in the English version?
				Giovanni - No, I don't think so.
				
				Weslley - You said that you think there isn't any issue, or any specific tool that is easier to be found in the Portugues version than in the English one.
				Giovanni - I did not face this situation. I didn't find anything that I could say "Just in Portuguese version.". Take 'Lua' for example, it is developed by Puc. It is extremely widespread in Brazil and even the official documentation is in English.
				
				Weslley - Do you know anyone who use the Portuguese version of StackOverflow?
				Giovanni - I only know one person, but he had the same situation that I did: few content.
				
				Weslley - Why do you think there is a small number of people using the stackoverflow in Portuguese? You know many people who work in computer science, but among these people, you only know one who used the Portuguese version. Why do you think that?
				Giovanni - Some people did not even know that there is a Portuguese version. And I think that the fact the English version is older and has more content, everyone goes directly to the English version.
				
				Weslley - Do you think that something can be done to to make people use more the Portuguese version? What I get it is that you are saying that people do not use the Portuguese version (you and your friend), because of the small number of content, it has few questions, few answers, etc .. do you think that anything can be made so people can use more the Portuguese version?
				Giovanni - A not accurate try, would be to make the automatic translation from one to the other.
				
				Weslley - You say, to get all the content of the Englsih version and move them to the Portuguese version? is it?
				Giovanni - Yes, That's it.
				
				Weslley - Do you think that even those people who do not speak English, they are using the English version with the help of a translator or an online translation tool?
				Giovanni - Yes, I do.
				
				Weslley - Do you know anyone who does not speak English, but use some translation tool to translate from Portuguese to English, and use it on StackOverflow?
				Giovanni - I know people who do not speak English very well, people that would not be able to read the texts in English, but they use it anyway. But they are isolated cases.
				
				Weslley - Do you know if these people make questions? How is the behavior of these people, they ask questions, they answer?
				Giovanni - No, I don't have this information, but I believe they only search.
				
				Weslley - In your opinion, what is the importance of having the StackOverflow in Portuguese? Do you judge that it is important?
				Giovanni - Yes, I prefer tools in Portuguese, because in this way I can avoid misunderstood. But as I said, It is a new tool and the fact that there is few content, it make it difficult to use it. I mean, when you access the StackOverflow in Portuguese, and you do not find what you want or find it but not the all package, you get frustrated and you will not access it anymore.
				
				Weslley - So you think it's important to have the Portuguese version?
				Giovanni - Yes.
				
				Weslley - Ok Giovanni, that's today only. I appreciate your participation, if we need anything else we will get in touch with you. Thank you!
				Giovanni - Thank you!
				
				Weslley - Bye!
				Giovanni - Bye!
				
				
//...
				
				Second part of the interview
				
				
..//
				
				
				Weslley - Hello Giovanni! We stil have some questions, do you mind answering a few more questions regarding to the StackOverflow?
				Giovanni - Hello Weslley! No, I don't mind, I can answer them.
				
				Weslley - Did you find some question on StackOverflow in Portuguese that you knew the answer?
				Giovanni - No, the results for the search that I usually made were related to what I was looking for. Thus, I didn't know the answer.
				
				Weslley - But have you searched for any questions that you already knew the answer, just to try to answer them?
				Giovanni - I was considering to contribute to the StackOverflow in Portuguese. I decided to do so, after our last interview, but I must confess I have not started yet.
				
				Weslley - That would be my next question, I remember that you criticized the small amount of content on the Portugues version of StackOverflow. What do you think about creating new content, asking and answering them, perhaps translating the content available in English to Portuguese?
				Giovanni - I got the same idea, I found this idea very interesting. 
				
				Weslley - Do you feel motivated to help other people in StackOverflow?
				Giovanni - Yes!
				
				Weslley - You feel motivated to help others on StackOverflow, but you never helped. Why?
				Giovanni - Usually I make some search in StackOverflow just to solve my problems. It never came to my mind, that I could help others. I had already contributed in mailing list, but I had never thought that I could contribute to the StackOverflow.
				
				Weslley - Which mailing list did you participate?
				Giovanni - Iptables and shellscript from yahoo, and ipv6 from cgi.br.
				
				Weslley - So you already helped the community, I mean, you used to contribute to the mailing list of these projects, but you have never contributed to the StackOverflow. Do you have any specific reason for not contributed to the StackOverflow?
				Giovanni - I am not able to identify a specific reason, but I think the reason might be inattention, carelessness.
				
				Weslley - Do you feel more motivated to participate in the mailing list than in StackOverflow?
				Giovanni - I don't think so. The amount of email is quite big, I have to read them to check how I can help them. Thus, I think the work would be quite the same at StackOverflow.
				
				Weslley - Hum, ok! Will you continue access the Portugues version of StackOverflow or did you give it up?
				Giovanni - I will access the Portugues version and I have plans to try to answer one question per day.
				
				Weslley - When you have to solve a problem, in which version of the StackOverflow do you do the search?
				Giovanni - First in Portuguese, if I get no answer, then I access the English version.
				
				Weslley - So, until now, the Portuguese version is your first option.
				Giovanni - yes. Since when I knew this service was available, I preferer the Portuguese version.
				
				Weslley - Do you feel motivated to help the StackOverflow in English?
				Giovanni - I think so, but I will try to help the Portuguese version first.
				
				Weslley - So, do you feel more motivated to help the Portuguese version than English one?!
				Giovanni - yes!
				
				Weslley - Would you know why?
				Giovanni - The English version is older and it has more people contributing. So, I think it would be more useful if I try to help people who use the Portuguese version.
				
				Weslley - Which language was used in mailing list?
				Giovanni - It had not an oficial language, you could see several languages.
				
				Weslley - Which language did you participate?
				Giovanni - Portuguese and English.
				
				Weslley - Ok! I think that is all for today. Thank you again for your help.
				Giovanni - You are welcome.
				
				Weslley - Bye.
				Giovanni - Bye.